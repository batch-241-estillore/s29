//find for first name s or last name d but display first, last only and exclude ID
db.users.find(
	{$or: 
	[
		{firstName: {$regex: "s", $options: '%i'}},
		{lastName: {$regex:"d", $options: '%i'}}
	]},
	{
		firstName: 1,
		lastName: 1,
		_id: 0
	}
	)


//find for users that are hr dept and age gte 70
db.users.find({
	$and: 
	[
		{department:{$regex: 'HR', $options: '%i'}},
		{age: {$gte: 70}}
	]
});


//find useres with letter e in firstname and age lte 30
db.users.find({
	$and: 
	[
		{firstName:{$regex: 'e', $options: '%i'}},
		{age: {$lte: 30}}
	]
});




